﻿using System;
using System.Linq;
using System.Reflection;

namespace CsvHelper.Extend
{
    public static class ObjectUtils
    {
        public static T GetAttribute<T>(this ICustomAttributeProvider provider, bool inherit = false)
where T : Attribute
        {
            return provider
                .GetCustomAttributes(typeof(T), inherit)
                .OfType<T>()
                .FirstOrDefault();
        }
    }
}
