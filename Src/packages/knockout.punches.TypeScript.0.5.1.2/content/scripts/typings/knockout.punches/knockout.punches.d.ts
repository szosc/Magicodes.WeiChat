/// <reference path="../knockout/knockout.d.ts"/>

// http://mbest.github.io/knockout.punches/

interface KnockoutStatic {
    punches: KnockoutPunches;
    filters: KnockoutPunchesFilters;
}

interface KnockoutPunches {
    enableAll(): void;

    utils: KnockoutPunchesUtils;
    interpolationMarkup: KnockoutPunchesInterpolationMarkup;
    attributeInterpolationMarkup: KnockoutPunchesAttributeInterpolationMarkup;
    textFilter: KnockoutPunchesTextFilter;
    namespacedBinding: KnockoutPunchesNamespacedBinding;
    wrappedCallback: KnockoutPunchesWrappedCallback;
    expressionCallback: KnockoutPunchesExpressionCallback;
    preprocessBindingProperty: KnockoutPunchesPreprocessBindingProperty;
}

interface KnockoutPunchesUtils {
    setBindingPreprocessor(bindingKeyOrHandler, preprocessFn): void;
    setNodePreprocessor(preprocessFn): void;
}

interface KnockoutPunchesInterpolationMarkup {
    enable(): void;
    wrapExpression: (expressionText: string, node: Node) => Node[];
    preprocessor(node): void;
}

interface KnockoutPunchesAttributeInterpolationMarkup {
    enable(): void;
    preprocessor(node): void;
    attributeBinding(name, value, node): void;
}

interface KnockoutPunchesTextFilter {
    preprocessor(input): void;
    enableForBinding(bindingKeyOrHandler): void;
}

interface KnockoutPunchesNamespacedBinding {
    enableForBinding(bindingKeyOrHandler): void;
    preprocessor(input): void;
    setDefaultBindingPreprocessor(namespace, preprocessFn): void;
    defaultGetHandler(name, namespace, namespacedName): void;
}

interface KnockoutPunchesWrappedCallback {
    preprocessor(input): void;
    enableForBinding(bindingKeyOrHandler): void;
}

interface KnockoutPunchesExpressionCallback {
    makePreprocessor(args): void;
    eventPreprocessor(input): void;
    enableForBinding(bindingKeyOrHandler, args): void;
}

interface KnockoutPunchesPreprocessBindingProperty {
    setPreprocessor(bindingKeyOrHandler, property, preprocessFn): void;
}

interface KnockoutPunchesFilters {
    setPreprocessor(bindingKeyOrHandler, property, preprocessFn): void;
}
