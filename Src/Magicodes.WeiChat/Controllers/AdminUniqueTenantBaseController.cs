﻿using Magicodes.WeiChat.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Controllers
{
    /// <summary>
    /// 此控制器主要用于配置代码设置
    /// </summary>
    /// <typeparam name="TEntry"></typeparam>
    [Authorize]
    public class AdminUniqueTenantBaseController<TEntry> : TenantBaseController<TEntry>
        where TEntry : WeiChat_AdminUniqueTenantBase<int>, new()
    {
        [HttpGet]
        public virtual ActionResult Index()
        {
            var model = db.Set<TEntry>().FirstOrDefault();
            if (model == null)
            {
                model = new TEntry()
                {
                    CreateBy = UserId,
                    CreateTime = DateTime.Now,
                    TenantId = TenantId
                };
                db.Set<TEntry>().Add(model);
                db.SaveChanges();
            }
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult Index(TEntry model)
        {
            if (ModelState.IsValid)
            {
                SetModelWithSaveChanges(model, model.Id);
            }
            return View(model);
        }
    }
}