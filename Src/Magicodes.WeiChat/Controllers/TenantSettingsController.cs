﻿using Magicodes.WeiChat.ComponentModel.Setting;
using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Data.Models.Settings;
using Magicodes.WeiChat.Infrastructure.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Controllers
{
    public class TenantSettingsController : TenantBaseController<WeiChat_App>
    {
        SettingManager settingManager = SettingManager.Current;
        // GET: TenantSettings
        public ActionResult Index()
        {
            var groups = settingManager.GetTenantSettingGroup(TenantId);
            if (!groups.Any(p => p.Name == "站点设置"))
            {
                settingManager.AddOrUpdateTenantGroupSetting(new App_SettingGroup()
                {
                    Description = "设置网站基础信息以及相关配置",
                    IsVisibleToClients = false,
                    Name = "站点设置",
                    Scopes = SettingScopes.Application,
                    DisplayName = "站点设置"
                }, TenantId);
                settingManager.AddOrUpdateTenantGroupSetting(new App_SettingGroup()
                {
                    Description = "设置服务器事件的开关",
                    IsVisibleToClients = false,
                    Name = "服务器事件设置",
                    Scopes = SettingScopes.Application,
                    DisplayName = "服务器事件设置"
                }, TenantId);
            }
            return View();
        }
        [HttpGet]
        public ActionResult GetGroups()
        {
            return Json(settingManager.GetTenantSettingGroup(TenantId), JsonRequestBehavior.AllowGet);
        }
    }
}