﻿using Magicodes.WeiChat.Data.Models;
using Magicodes.WeChat.SDK;
using Magicodes.WeiChat.Models;
using Senparc.Weixin;
using Senparc.Weixin.Exceptions;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.CustomService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webdiyer.WebControls.Mvc;

namespace Magicodes.WeiChat.Controllers
{
    [Authorize]
    public class CustomController : TenantBaseController<WeiChat_KFCInfo>
    {
        // GET: Custom
        public ActionResult Index(int pageIndex = 1, int pageSize = 20)
        {
            var data = CustomServiceApi.GetCustomBasicInfo(AccessToken);
            var dataList = data.kf_list
                .Select(p =>
                    new CustomViewModel()
                    {
                        Id = p.kf_id,
                        NickName = p.kf_nick,
                        Account = p.kf_account,
                        HeadImgUrl = p.kf_headimgurl
                    });
            var pagedList = new PagedList<CustomViewModel>(dataList, pageIndex, pageSize, dataList.Count());
            return View(pagedList);
        }
        public ActionResult Add()
        {
            return View();
        }
        [Route("Edit/{id}")]
        [HttpGet]
        public ActionResult Edit(string id)
        {
            var kf = db.WeiChat_KFCInfos.FirstOrDefault(p => p.Account == id);
            if (kf == null)
            {
                ModelState.AddModelError("", "该账号不存在！");
            }
            else
            {
                var addModel = new AddCustomViewModel()
                {
                    Account = kf.Account.Split('@')[0],
                    NickName = kf.NickName
                };
                return View(addModel);
            }
            return View(new AddCustomViewModel());
        }
        [Route("Edit/{id}")]
        [HttpPost]
        public ActionResult Edit(string id, AddCustomViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {

                var result = CustomServiceApi.UpdateCustom(
                   AccessToken,
                   id,
                   model.NickName,
                   model.Password);
                if (result.errcode != ReturnCode.请求成功)
                {
                    ViewBag.Messages = new List<MessageInfo>
                        {
                            new MessageInfo(){
                                Message=result.errmsg,
                                MessageType=MessageTypes.Danger
                            }
                        };
                    return View(model);
                }
                else
                {
                    var kf = db.WeiChat_KFCInfos.First(p => p.Account == id);
                    kf.NickName = model.NickName;
                    db.SaveChanges();
                }
            }
            catch (ErrorJsonResultException ex)
            {
                if (ex.JsonResult.errcode == ReturnCode.客服帐号已存在kf_account_exsited)
                {
                    ViewBag.Messages = new List<MessageInfo>                {
                        new MessageInfo(){
                            Message="客服账号已存在！",
                            MessageType=MessageTypes.Danger
                        }
                    };
                    return View(model);
                }
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AddCustomViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {

                var result = WeChatApisContext.Current.CustomerServiceApi.AddCustomerAccount(
                   model.Account,
                   model.NickName,
                   model.Password);
                if (!result.IsSuccess())
                {
                    ViewBag.Messages = new List<MessageInfo>
                        {
                            new MessageInfo(){
                                Message=result.GetFriendlyMessage(),
                                MessageType=MessageTypes.Danger
                            }
                        };
                    return View(model);
                }
                else
                {
                    var kf = new WeiChat_KFCInfo()
                    {
                        Account = model.Account + "@" + WeChatConfigManager.Current.GetConfig().WeiXinAccount,
                        HeadImgUrl = "",
                        NickName = model.NickName
                    };
                    db.WeiChat_KFCInfos.Add(kf);
                    db.SaveChanges();
                }
            }
            catch (ErrorJsonResultException ex)
            {
                if (ex.JsonResult.errcode == ReturnCode.客服帐号已存在kf_account_exsited)
                {
                    ViewBag.Messages = new List<MessageInfo>                {
                        new MessageInfo(){
                            Message="客服账号已存在！",
                            MessageType=MessageTypes.Danger
                        }
                    };
                    return View(model);
                }
                else if (ex.JsonResult.errcode == ReturnCode.客服帐号个数超过限制)
                {
                    ViewBag.Messages = new List<MessageInfo>                {
                        new MessageInfo(){
                            Message="客服帐号个数超过限制！",
                            MessageType=MessageTypes.Danger
                        }
                    };
                    return View(model);
                }
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Remove(RemoveCustomViewModel model)
        {
            var message = new MessageInfo()
            {
                Message = "操作成功！",
                MessageType = MessageTypes.Success
            };
            if (!ModelState.IsValid)
            {
                message.Message = "客服账号输入有误！";
                message.MessageType = MessageTypes.Danger;
                return Json(message);
            }
            try
            {
                var result = WeChatApisContext.Current.CustomerServiceApi.RemoveCustomerAccount(model.Account);

                if (!result.IsSuccess())
                {
                    message.Message = result.GetFriendlyMessage();
                    message.MessageType = MessageTypes.Danger;
                }
            }
            catch (ErrorJsonResultException ex)
            {
                message.Message = ex.Message;
                message.MessageType = MessageTypes.Danger;
            }
            return Json(message);
        }
    }
}