﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Magicodes.WeiChat.Infrastructure.MvcExtension.Ajax;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Webdiyer.WebControls.Mvc;
using Magicodes.Mvc.AuditFilter;
using Magicodes.Mvc.RoleMenuFilter;
using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Data.Models.Log;

namespace Magicodes.WeiChat.Controllers.WeChat
{
    public class Log_AuditController : TenantBaseController<Log_Audit>
    {

        // GET: Log_Audit
        [AuditFilter("审计日志查询", "9ba726ab-7a84-4ec0-93c6-09f7c8928118")]
        [RoleMenuFilter("审计日志", "9ba726ab-7a84-4ec0-93c6-09f7c8928118", "Admin",
             url: "/Log_Audit", parentId: "F348A29F-9F4C-476A-9932-23C607A4F9FB")]
        public ActionResult Index(string q, int pageIndex = 1, int pageSize = 10)
        {
            var queryable = db.Log_Audits.AsQueryable();
            if (!string.IsNullOrWhiteSpace(q))
            {
                //请替换为相应的搜索逻辑
                queryable = queryable.Where(p => p.Code.Contains(q) || p.Remark.Contains(q) || p.RequestUrl.Contains(q) || p.Title.Contains(q) || p.ClientIpAddress.Contains(q) || p.ClientName.Contains(q) || p.CreateBy.Contains(q));
            }
            var pagedList = new PagedList<Log_Audit>(
                             queryable.OrderBy(p => p.Id)
                             .Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList(),
                             pageIndex, pageSize, queryable.Count());
            return View(pagedList);
        }

        // GET: Log_Audit/Details/5
        [AuditFilter("审计日志详细", "a2d938f1-da57-4f67-b392-33d43248f8af")]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Log_Audit log_Audit = db.Log_Audits.Find(id);
            if (log_Audit == null)
            {
                return HttpNotFound();
            }
            return View(log_Audit);
        }

        // GET: Log_Audit/Delete/5
        [AuditFilter("审计日志待删除", "3abd7a46-64d0-4baf-8422-de5f22b93b36")]
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Log_Audit log_Audit = db.Log_Audits.Find(id);
            if (log_Audit == null)
            {
                return HttpNotFound();
            }
            return View(log_Audit);
        }

        // POST: Log_Audit/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [AuditFilter("审计日志删除", "66223bcf-6a8d-4aae-a4f8-aa57ddb86b8c")]
        public ActionResult DeleteConfirmed(long? id)
        {
            Log_Audit log_Audit = db.Log_Audits.Find(id);
            db.Log_Audits.Remove(log_Audit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // POST: Log_Audit/BatchOperation/{operation}
        /// <summary>
        /// 批量操作
        /// </summary>
        /// <param name="operation">操作方法</param>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        [HttpPost]
        [Route("Log_Audit/BatchOperation/{operation}")]
        [AuditFilter("审计日志批量操作", "0b83b4cb-f198-400c-85d9-c26cf92e8a64")]
        public ActionResult BatchOperation(string operation, params long?[] ids)
        {
            var ajaxResponse = new AjaxResponse();
            if (ids.Length > 0)
            {
                try
                {
                    var models = db.Log_Audits.Where(p => ids.Contains(p.Id)).ToList();
                    if (models.Count == 0)
                    {
                        ajaxResponse.Success = false;
                        ajaxResponse.Message = "没有找到匹配的项，项已被删除或不存在！";
                        return Json(ajaxResponse);
                    }
                    switch (operation.ToUpper())
                    {
                        case "DELETE":
                            #region 删除
                            {
                                db.Log_Audits.RemoveRange(models);
                                db.SaveChanges();
                                ajaxResponse.Success = true;
                                ajaxResponse.Message = string.Format("已成功操作{0}项！", models.Count);
                                break;
                            }
                        #endregion
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    ajaxResponse.Success = false;
                    ajaxResponse.Message = ex.Message;
                }
            }
            else
            {
                ajaxResponse.Success = false;
                ajaxResponse.Message = "请至少选择一项！";
            }
            return Json(ajaxResponse);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
