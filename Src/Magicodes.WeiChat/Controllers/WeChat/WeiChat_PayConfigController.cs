﻿using Magicodes.WeiChat.Data.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Controllers
{
    /// <summary>
    /// 微信支付配置
    /// </summary>
    public class WeiChat_PayConfigController : AdminUniqueTenantBaseController<WeiChat_Pay>
    {
        /// <summary>
        /// 获取支付通知地址
        /// </summary>
        /// <returns></returns>
        private string GetNotifyUrl()
        {
            return string.Format("{0}://{1}/WeiChat/PayNotify/{2}", Request.Url.Scheme, Request.Url.Host, TenantId);
        }
        public override ActionResult Index()
        {
            var model = db.Set<WeiChat_Pay>().FirstOrDefault();
            if (model == null)
            {
                model = new WeiChat_Pay()
                {
                    CreateBy = UserId,
                    CreateTime = DateTime.Now,
                    TenantId = TenantId,
                    Notify = GetNotifyUrl()
                };
                db.Set<WeiChat_Pay>().Add(model);
                db.SaveChanges();
            }
            else
            {
                if (model.Notify != GetNotifyUrl())
                {
                    model.Notify = GetNotifyUrl();
                    db.SaveChanges();
                }
            }
            return View(model);
        }
        [HttpPost]
        public override ActionResult Index(WeiChat_Pay model)
        {
            if (Request.Files.Count > 0 && Request.Files["PayCertFile"].ContentLength > 0 && Path.GetExtension(Request.Files["PayCertFile"].FileName).ToLower() != ".p12")
            {
                ModelState.AddModelError("PayCertFile", "上传的支付证书文件有误！");
            }
            if (string.IsNullOrWhiteSpace(model.CertPassword))
            {
                ModelState.AddModelError("CertPassword", "证书密钥是必须的！");
            }
            if (string.IsNullOrWhiteSpace(model.MchId))
            {
                ModelState.AddModelError("MchId", "商户Id是必须的！");
            }
            if (string.IsNullOrWhiteSpace(model.Notify))
            {
                ModelState.AddModelError("Notify", "回调地址是必须的！");
            }
            if (string.IsNullOrWhiteSpace(model.TenPayKey))
            {
                ModelState.AddModelError("TenPayKey", "支付密钥是必须的！");
            }
            if (ModelState.IsValid)
            {
                if (Request.Files.Count > 0 && Request.Files["PayCertFile"].ContentLength > 0 && Path.GetExtension(Request.Files["PayCertFile"].FileName).ToLower() == ".p12")
                {
                    var file = Request.Files["PayCertFile"];
                    #region 保存证书，用于红包发放等接口
                    var path = Path.Combine(Server.MapPath("~/App_Data"), "certs", TenantId.ToString());
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    path = Path.Combine(path, "pay.p12");
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    file.SaveAs(path);
                    #endregion
                    model.PayCertPath = string.Format("/App_Data/certs/{0}/pay.p12", TenantId);
                }
                model.Notify = GetNotifyUrl();
                SetModelWithSaveChanges(model, model.Id);
            }
            return View(model);
        }
    }
}