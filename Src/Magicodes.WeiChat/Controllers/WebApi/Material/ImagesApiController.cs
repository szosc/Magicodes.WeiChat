﻿using Senparc.Weixin.MP.AdvancedAPIs.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Senparc.Weixin;
using System.Net;
using Senparc.Weixin.MP.AdvancedAPIs;

namespace Magicodes.WeiChat.Controllers.WebApi
{
    [RoutePrefix("api/images")]
    public class ImagesApiController : WebApiControllerBase
    {
        [Route("{id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(string id)
        {
            var result = MediaApi.DeleteForeverMedia(AccessToken, id);
            if (result.errcode != ReturnCode.请求成功)
            {
                return BadRequest(result.errmsg);
            }
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}