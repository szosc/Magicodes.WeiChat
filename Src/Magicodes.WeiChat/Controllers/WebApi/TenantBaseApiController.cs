﻿using EntityFramework.DynamicFilters;
using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Data.Models.Interface;
using Magicodes.WeChat.SDK;
using Magicodes.WeiChat.Infrastructure.MvcExtension.Results;
using Magicodes.WeiChat.Infrastructure.Tenant;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Controllers.WebApi
{
    public class TenantBaseApiController<TEntry> : WebApiControllerBase
        where TEntry : class, ITenantId, new()
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            var tenantId = TenantId;
            TenantManager.Current.EnableTenantFilter(db, tenantId);
            base.Initialize(controllerContext);
        }
    }
}