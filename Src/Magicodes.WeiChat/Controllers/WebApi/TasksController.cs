﻿using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Data.Models.Site;
using Magicodes.WeiChat.Helpers;
using Magicodes.WeiChat.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Magicodes.WeiChat.Controllers.WebApi
{
    [RoutePrefix("api/Tasks")]
    public class TasksController : WebApiControllerBase
    {
        [HttpGet]
        [Route("StartTenantTask/{type}")]
        public IHttpActionResult StartTenantTask(WeiChat_SyncTypes type)
        {
            TaskHelper.StartTask(type);
            return Ok();
        }
    }
}