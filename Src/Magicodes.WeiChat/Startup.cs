﻿using Microsoft.Owin;
using Owin;
using System.Configuration;
using System.Web;

[assembly: OwinStartupAttribute(typeof(Magicodes.WeiChat.Startup))]
namespace Magicodes.WeiChat
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //判断是否启用SignalR
            if (ConfigurationManager.AppSettings["EnableSignalR"] != null && ConfigurationManager.AppSettings["EnableSignalR"].ToLower() == "true")
            {
                app.MapSignalR();
            }
            
        }
    }
}
