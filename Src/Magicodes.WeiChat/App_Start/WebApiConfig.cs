﻿using Magicodes.WeiChat.Infrastructure.WebApiExtension.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Magicodes.WeiChat
{
    public static class WebApiConfig
    {
        internal static readonly string UrlPrefixRelative="~/api/";

        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Filters.Add(new WebApiExceptionFilter());
        }
    }
}
