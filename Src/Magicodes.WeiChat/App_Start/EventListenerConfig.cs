﻿using Magicodes.Logger.NLog;
using Magicodes.WeiChat.Infrastructure.Events;
using Senparc.Weixin.MP.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Magicodes.WeiChat.App_Start
{
    public class EventListenerConfig
    {
        public static void Register()
        {
            EventManager.Current.Logger = new NLogLogger("EventManagerLogger");

            ////以下代码仅供参考,具体请查看单元测试项目
            //事件定义以及模板消息操作定义(支持表达式和变量)
            //EventManager.Current.Listeners = new List<EventListener>()
            //{
            //    new EventListener()
            //    {
            //        Name=typeof(TestData).FullName,
            //        DisplayName="测试事件",
            //        Enabled=true,
            //        Actions=new List<EventActionBase>()
            //        {
            //            //{{first.DATA}}
            //            //会员昵称：{{keyword1.DATA}}
            //            //关注时间：{{keyword2.DATA}}
            //            //{{remark.DATA}}
            //            new MessagesTemplateAction()
            //            {
            //                MessagesTemplateId ="riid7aad8OKRQD9Ey6dclWBBkrqZSFDhlpKh0_spGLA",
            //                Items=new Dictionary<string, MessageTemplateValue>()
            //                {
            //                    {"first",new MessageTemplateValue()
            //                        {
            //                            Expression="{\"感谢关注：\"+{0}.GetWeChatUserByOpenId({0}.WeChatUser.OpenId).NickName}"
            //                        }
            //                    },
            //                    {"keyword1",new MessageTemplateValue()
            //                        {
            //                            Expression="{{0}.WeChatUser.NickName}"
            //                        }
            //                    },
            //                    {"keyword2",new MessageTemplateValue()
            //                        {
            //                            Expression="{DateTime.Now.ToString(\"yyyy-MM-dd HH:mm:ss\")}"
            //                        }
            //                    },
            //                    {"remark",new MessageTemplateValue()
            //                        {
            //                            Expression="{\"感谢您的关注\"+{0}.GetWeChatUserByOpenId({0}.WeChatUser.OpenId).NickName}"
            //                        }
            //                    },
            //                },
            //                Receivers="{{0}.WeChatUser.OpenId}",
            //                Url="http://xin-lai.com"
            //            }
            //        }
            //    }
            //};
            //
            //触发示例代码
            //===================================================
            //EventManager.Current.Trigger(new TestData()
            //{
            //    WeChatUser = new WeiChat_User()
            //    {
            //        OpenId = "oXELNwzZgamuLS0JrJhVgdelzKyw",
            //        NickName = "雪雁"
            //    }
            //});
            //====================================================
        }
    }
}