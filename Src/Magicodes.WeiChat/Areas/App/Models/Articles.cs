﻿using Magicodes.WeiChat.Data.Models.Site;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Magicodes.WeiChat.Areas.App.Models
{
    public class Articles:Site_Article
    {
        /// <summary>
        /// ID
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// 分类名
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 当前分类下的文章数
        /// </summary>
        public int Count { get; set; }
    }
}