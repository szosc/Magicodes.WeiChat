﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace Magicodes.WeiChat.Areas.App.Hubs
{
    public class TestHub : Hub
    {
        /// <summary>
        /// 微信端消息推送
        /// </summary>
        /// <param name="name">昵称</param>
        /// <param name="message">消息</param>
        public void WeiChatSend(string name, string message)
        {
            Clients.All.weiChatClick(name, message);
        }
        /// <summary>
        /// Windows客户端消息推送
        /// </summary>
        /// <param name="name">昵称</param>
        /// <param name="message">消息</param>
        public void WindowsSend(string name, string message)
        {
            Clients.All.addToWeiChatMessage(name, message);
        }
    }
}