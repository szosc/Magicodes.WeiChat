﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Magicodes.WeiChat.Unity;

namespace Magicodes.WeiChat
{
    public static class HMTLHelperExtensions
    {
        /// <summary>
        /// 是否为当前菜单
        /// </summary>
        /// <param name="html"></param>
        /// <param name="controller"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static string IsSelected(this HtmlHelper html, string controller = null, string action = null)
        {
            string cssClass = "active";
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];

            if (String.IsNullOrEmpty(controller))
                controller = currentController;

            if (String.IsNullOrEmpty(action))
                action = currentAction;

            return controller == currentController && action == currentAction ?
                cssClass : String.Empty;
        }
        /// <summary>
        /// 允许选择多个控制器
        /// </summary>
        /// <param name="html"></param>
        /// <param name="controllerStr"></param>
        /// <returns></returns>
        public static string IsSelectesControllers(this HtmlHelper html, string controllerStr)
        {
            var controllers = controllerStr.Split(',');
            foreach (var item in controllers)
            {
                if (html.IsSelected(item) == "active")
                {
                    return "active";
                }
            }
            return string.Empty;
        }
        public static string PageClass(this HtmlHelper html)
        {
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            return currentAction;
        }

        public static string IsSelectesUrl(this HtmlHelper html, string url)
        {
            return html.ViewContext.HttpContext.Request.Url.AbsoluteUri.IndexOf(url, StringComparison.CurrentCultureIgnoreCase) == -1 ? "" : "active";
        }
        /// <summary>
        /// 显示枚举
        /// </summary>
        /// <param name="html"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string DisplayForEnum(this HtmlHelper html, Enum value)
        {
            return value.GetEnumMemberDisplayName();
        }
    }
}
