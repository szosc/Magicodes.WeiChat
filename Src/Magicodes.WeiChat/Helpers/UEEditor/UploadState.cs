﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magicodes.WeiChat.Helpers.UEEditor
{
    public enum UploadState
    {
        Success = 0,
        SizeLimitExceed = -1,
        TypeNotAllow = -2,
        FileAccessError = -3,
        NetworkError = -4,
        Unknown = 1,
        /// <summary>
        /// 微信API调用次数超过限额
        /// </summary>
        WeChatApiLimit = -5
    }
}
