﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace Magicodes.WeiChat.Helpers
{
    public enum ShopEvents
    {
        Product,
        Order
    }
    public enum Actions
    {
        Add,
        Edit,
        Remove
    }

    public static class NotifyHelper
    {
        public static Dictionary<int, string> NotifyUrls { get; set; }
        /// <summary>
        /// 通知转发处理
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="eventType"></param>
        /// <param name="action"></param>
        /// <param name="data"></param>
        public static void Notify(int tenantId, ShopEvents eventType, Actions action, object data)
        {
            if (NotifyUrls.ContainsKey(tenantId))
            {
                Task.Run(() =>
                {
                    using (var client = new HttpClient())
                    {
                        HttpResponseMessage result = null;
                        client.BaseAddress = new Uri(NotifyUrls[tenantId]);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var apiResult = client.PostAsJsonAsync(NotifyUrls[tenantId], new { Event = eventType.ToString(), TenantId = tenantId, Action = action.ToString(), Data = data }).Result;
                        if (apiResult.StatusCode == HttpStatusCode.OK)
                        {

                        }
                    }
                });
            }
        }
    }
}