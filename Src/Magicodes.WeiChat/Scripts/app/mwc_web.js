/**
 * @namespace nwc
 */
var mwc;
(function (mwc) {
    var log;
    (function (log) {
        /**
        * 枚举：日志等级
        */
        (function (levels) {
            levels[levels["DEBUG"] = 1] = "DEBUG";
            levels[levels["INFO"] = 2] = "INFO";
            levels[levels["WARN"] = 3] = "WARN";
            levels[levels["ERROR"] = 4] = "ERROR";
            levels[levels["FATAL"] = 5] = "FATAL";
        })(log.levels || (log.levels = {}));
        var levels = log.levels;
        ;
    })(log || (log = {}));
})(mwc || (mwc = {}));
//# sourceMappingURL=mwc_web.js.map