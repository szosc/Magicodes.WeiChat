﻿/**
 * @namespace nwc
 */
module mwc {
    /**
    * 设置
    */
    interface ISetting {
        /**
        * 获取值
        */
        getValue(name: string): string;
        /**
        * 获取值
        */
        setValue(name: string, value: string);
    }
    module log {
        /**
        * 枚举：日志等级
        */
        export enum levels {
            DEBUG = 1,
            INFO = 2,
            WARN = 3,
            ERROR = 4,
            FATAL = 5
        };
        /**
        * 枚举：日志接口
        */
        interface ILog {
            /**
            * 记录日志
            * @logObject 日志对象
            * @logLevel 日志等级
            */
            log(logObject: any, logLevel: log.levels);
            /**
            * 记录Debug日志
            * @logObject 日志对象
            */
            debug(logObject: any);
            /**
            * 记录info日志
            * @logObject 日志对象
            */
            info(logObject: any);
            /**
            * 记录warn日志
            * @logObject 日志对象
            */
            warn(logObject: any);
            /**
           * 记录error日志
           * @logObject 日志对象
           */
            error(logObject: any);
            /**
           * 记录fatal日志
           * @logObject 日志对象
           */
            fatal(logObject: any);
        }
    }

    /**
    * 通知
    */
    interface INotify {
        /**
        * 成功通知
        */
        success(message: string, title: string): void;
        /**
        * 信息通知
        */
        info(message: string, title: string): void;
        /**
        * 警告通知
        */
        warn(message: string, title: string): void; 
        /**
        * 错误通知
        */
        error(message: string, title: string): void;
    }

    /**
    * 弹窗消息
    */
    interface IPopMessage {
        /**
        * 成功消息
        */
        success(message: string, title: string): any;
        /**
        * 信息消息
        */
        info(message: string, title: string): any;
        /**
        * 警告消息
        */
        warn(message: string, title: string): any; 
        /**
        * 错误消息
        */
        error(message: string, title: string): any;
        /**
        * 确认消息
        */
        confirm(message: string, titleOrCallback: any, callback: any): any;
    }

    interface IRestApi {
        /**
        * GET请求
        */
        get(setting: any): void;
        /**
       * GET请求
       */
        delete(setting: any): void;
        /**
       * GET请求
       */
        put(setting: any): void;
        /**
       * GET请求
       */
        post(setting: any): void;
    }
    /**
    * UI 操作
    */
    interface IUI {
        /**
        * 锁定
        */
        block(elm: any): void;
        /**
        * 解除锁定
        */
        unblock(elm: any): void;
        /**
        * 显示忙碌状态
        */
        setBusy(elm: any): void;
        /**
        * 清楚忙碌状态
        */
        clearBusy(elm: any): void;
    }
}


