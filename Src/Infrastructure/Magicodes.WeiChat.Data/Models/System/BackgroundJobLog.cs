﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models.System
{
    public class BackgroundJobLog
    {
        [MaxLength(200)]
        public string Title { get; set; }
        public string Remark { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; } 

    }
}
