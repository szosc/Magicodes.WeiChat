﻿using System.ComponentModel.DataAnnotations;
using Magicodes.WeChat.SDK;

namespace Magicodes.WeiChat.Data.Models
{
    /// <summary>
    /// 微信支付配置
    /// </summary>
    public class WeiChat_Pay : WeiChat_AdminUniqueTenantBase<int>, IWeChatPayConfig
    {
        /// <summary>
        /// 与微信商户平台商户MchID一致
        /// </summary>
        [Display(Name = "证书密钥")]
        public string CertPassword { get; set; }

        [Display(Name = "商户Mch_ID")]
        public string MchId { get; set; }
        [Display(Name = "证书相对路径")]
        public string PayCertPath { get; set; }
        [Display(Name = "支付密钥")]
        public string TenPayKey { get; set; }
        [Display(Name = "回调地址")]
        public string Notify { get; set; }
    }
}