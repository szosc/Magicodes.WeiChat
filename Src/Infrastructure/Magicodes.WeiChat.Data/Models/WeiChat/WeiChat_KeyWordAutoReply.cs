﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Magicodes.WeiChat.Data.Models.Interface;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 微信关键字自动答复
    /// </summary>
    public class WeiChat_KeyWordAutoReply : ITenantId, IAdminCreate<string>, IAdminUpdate<string>
    {
        public WeiChat_KeyWordAutoReply()
        {
            Id = Guid.NewGuid();
        }
        [Key]
        public Guid Id { get; set; }
        /// <summary>
        /// 关键字
        /// </summary>
        [Display(Name = "关键字")]
        [MaxLength(100)]
        [Required]
        public string KeyWord { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
        [MaxLength(128)]
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        [Display(Name = "创建者")]
        //[NotMapped]
        [ForeignKey("CreateBy")]
        public AppUser CreateUser { get; set; }

        /// <summary>
        /// 更新者
        /// </summary>
        [MaxLength(128)]
        public string UpdateBy { get; set; }
        /// <summary>
        /// 编辑者
        /// </summary>
        [Display(Name = "最后编辑")]
        //[NotMapped]
        [ForeignKey("UpdateBy")]
        public AppUser UpdateUser { get; set; }
        /// <summary>
        /// 匹配类型
        /// </summary>
        [Display(Name = "匹配类型")]
        public KeyWordMatchTypes MatchType { get; set; }
        /// <summary>
        /// 关键字类型
        /// </summary>
        [Display(Name = "类型")]
        public KeyWordContentTypes KeyWordContentType { get; set; }
        /// <summary>
        /// 内容Id
        /// </summary>
        [Display(Name = "内容")]
        public Guid? ContentId { get; set; }
        public int TenantId { get; set; }
        [Display(Name = "是否支持菜单事件关键字触发")]
        public bool AllowEventKey { get; set; }
    }
}