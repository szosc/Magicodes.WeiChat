﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Magicodes.WeiChat.Data.Models.Interface;

namespace Magicodes.WeiChat.Data.Models
{
    /// <summary>
    /// 微信用户组信息
    /// </summary>
    public class WeiChat_UserGroup : ITenantId
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        /// <summary>
        /// 分组id，由微信分配
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "分组id")]
        public int GroupId { get; set; }
        /// <summary>
        /// 分组名字，UTF8编码
        /// </summary>
        [Display(Name = "组名")]
        [Required]
        [MaxLength(30)]
        public string Name { get; set; }
        /// <summary>
        /// 分组内用户数量
        /// </summary>
        [Display(Name = "用户数量")]
        public int UsersCount { get; set; }
        public int TenantId { get; set; }
    }
}