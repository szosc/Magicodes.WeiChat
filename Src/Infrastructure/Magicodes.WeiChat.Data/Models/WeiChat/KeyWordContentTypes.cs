﻿using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 关键字回复类型
    /// 参考：http://mp.weixin.qq.com/wiki/14/89b871b5466b19b3efa4ada8e577d45e.html
    /// </summary>
    public enum KeyWordContentTypes
    {
        /// <summary>
        /// 文本
        /// </summary>
        [Display(Name = "文本")]
        Text = 0,
        /// <summary>
        /// 图片
        /// </summary>
        [Display(Name = "图片")]
        Image = 1,
        ///// <summary>
        ///// 音乐
        ///// </summary>
        //[Display(Name = "音乐")]
        //Music = 2,
        /// <summary>
        /// 语音
        /// </summary>
        [Display(Name = "语音")]
        Voice = 3,
        /// <summary>
        /// 视频
        /// </summary>
        [Display(Name = "视频")]
        Video = 4,
        /// <summary>
        /// 图文
        /// </summary>
        [Display(Name = "图文")]
        News = 5,
        /// <summary>
        /// 接入客服
        /// </summary>
        [Display(Name = "接入客服")]
        CustomerService = 6
    }
}