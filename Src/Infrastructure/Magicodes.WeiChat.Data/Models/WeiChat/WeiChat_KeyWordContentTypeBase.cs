﻿using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Data.Models.Interface;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    #region 关键字处理
    /// <summary>
    /// 微信关键字
    /// </summary>
    public class WeiChat_KeyWordContentTypeBase : ITenantId, IAdminCreate<string>, IAdminUpdate<string>
    {
        public WeiChat_KeyWordContentTypeBase()
        {
            Id = Guid.NewGuid();
        }
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
        [MaxLength(128)]
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        [Display(Name = "创建者")]
        //[NotMapped]
        [ForeignKey("CreateBy")]
        public AppUser CreateUser { get; set; }

        /// <summary>
        /// 更新者
        /// </summary>
        [MaxLength(128)]
        [Display(Name = "最后编辑")]
        public string UpdateBy { get; set; }
        /// <summary>
        /// 编辑者
        /// </summary>
        [MaxLength(256)]
        [Display(Name = "最后编辑")]
        [ForeignKey("UpdateBy")]
        //[NotMapped]
        public AppUser UpdateUser { get; set; }

        public int TenantId { get; set; }
    }

    #endregion
}
