﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 图文消息素材
    /// </summary>
    public class Material_News
    {
        [Key]
        [MaxLength(50)]
        public string Id { get; set; }
        /// <summary>
        /// 文件名称
        /// </summary>
        public string Title
        {
            get
            {
                return Articles.First().title;
            }
        }
        
        /// <summary>
        /// 图文页的URL，或者，当获取的列表是图片素材列表时，该字段是图片的URL
        /// </summary>
        public string Url {
            get
            {
                return Articles.First().url;
            }
        }
        public List<Material_Article> Articles { get; set; }
    }
}
