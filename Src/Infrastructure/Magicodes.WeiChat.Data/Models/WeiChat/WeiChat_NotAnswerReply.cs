﻿using Magicodes.WeiChat.Data.Models.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 答不上来配置
    /// </summary>
    public class WeiChat_NotAnswerReply : WeiChat_AdminUniqueTenantBase<Guid>
    {
        public WeiChat_NotAnswerReply()
        {
            Id = Guid.NewGuid();
        }
        /// <summary>
        /// 关键字类型
        /// </summary>
        [Display(Name = "类型")]
        public KeyWordContentTypes KeyWordContentType { get; set; }
        /// <summary>
        /// 内容Id
        /// </summary>
        [Display(Name = "内容")]
        public Guid? ContentId { get; set; }
    }
}
