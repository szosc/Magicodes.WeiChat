﻿using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 多条图文消息信息，默认第一个item为大图,注意，如果图文数超过10，则将会无响应
    /// </summary>
    public class WeiChat_KeyWordNewsArticle : WeiChat_TenantBase<long>
    {
        /// <summary>
        /// 图文消息标题
        /// </summary>
        [MaxLength(50)]
        [Display(Name = "图文消息标题")]
        public string Title { get; set; }
        /// <summary>
        /// 图文消息描述
        /// </summary>
        [MaxLength(200)]
        [Display(Name = "图文消息描述")]
        public string Description { get; set; }
        /// <summary>
        /// 图片链接，支持JPG、PNG格式，较好的效果为大图360*200，小图200*200
        /// </summary>
        [MaxLength(300)]
        [Display(Name = "图片链接")]
        public string PicUrl { get; set; }
        /// <summary>
        /// 点击图文消息跳转链接
        /// </summary>
        [MaxLength(500)]
        [Display(Name = "点击图文消息跳转链接")]
        public string Url { get; set; }

    }
}