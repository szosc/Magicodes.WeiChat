﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Magicodes.WeiChat.Data.Models.Interface;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 模板消息发送记录
    /// </summary>
    public class WeiChat_MessagesTemplateSendLog : ITenantId, IAdminCreate<string>
    {
        [Key]
        public long Id { get; set; }
        /// <summary>
        /// 批次号
        /// </summary>
        [Display(Name = "批次号")]
        public Guid BatchNumber { get; set; }
        /// <summary>
        /// 模板消息Id
        /// </summary>
        [Required]
        public int MessagesTemplateId { get; set; }
        /// <summary>
        /// 模板消息编号
        /// </summary>
        [MaxLength(50)]
        [Required]
        public string MessagesTemplateNo { get; set; }
        /// <summary>
        /// 模板消息
        /// </summary>
        [Display(Name = "模板消息")]
        [ForeignKey("MessagesTemplateId")]
        public WeiChat_MessagesTemplate MessagesTemplate { get; set; }
        /// <summary>
        /// 消息内容
        /// </summary>
        [Display(Name = "消息内容")]
        public string Content { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "发送时间")]
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 接收人
        /// </summary>
        public string ReceiverId { get; set; }
        /// <summary>
        /// 接收人
        /// </summary>
        [Display(Name = "接收人")]
        [NotMapped]
        public WeiChat_User Receiver { get; set; }
        /// <summary>
        /// 顶部颜色
        /// </summary>
        [MaxLength(50)]
        [Display(Name = "顶部颜色")]
        public string TopColor { get; set; }
        /// <summary>
        /// 链接
        /// </summary>
        [MaxLength(225)]
        [Display(Name = "链接")]
        public string Url { get; set; }
        /// <summary>
        /// 发送结果
        /// </summary>
        [MaxLength(500)]
        [Display(Name = "发送结果")]
        public string Result { get; set; }
        /// <summary>
        /// 是否发送成功
        /// </summary>
        [Display(Name = "是否发送成功")]
        public bool IsSuccess { get; set; }
        public int TenantId { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
        [MaxLength(128)]
        public string CreateBy { get; set; }
    }
}