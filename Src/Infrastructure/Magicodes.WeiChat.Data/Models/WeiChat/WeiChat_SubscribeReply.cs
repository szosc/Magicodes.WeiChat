using Magicodes.WeiChat.Data.Models.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 关注时回复
    /// </summary>
    public class WeiChat_SubscribeReply : WeiChat_AdminUniqueTenantBase<Guid>
    {
        public WeiChat_SubscribeReply()
        {
            Id = Guid.NewGuid();
        }
        /// <summary>
        /// 关键字类型
        /// </summary>
        [Display(Name = "类型")]
        public KeyWordContentTypes KeyWordContentType { get; set; }
        /// <summary>
        /// 内容Id
        /// </summary>
        [Display(Name = "内容")]
        public Guid ContentId { get; set; }
    }
}
