﻿namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 微信视频回复关键字
    /// </summary>
    public class WeiChat_KeyWordVideoContent : WeiChat_KeyWordContentTypeBase
    {
        public string MediaId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

    }
}