﻿using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 性别类型
    /// </summary>
    public enum SexTypes
    {
        [Display(Name = "所有")]
        All = 0,
        [Display(Name = "未知")]
        UnKnown = 1,
        [Display(Name = "男")]
        Man = 2,
        [Display(Name = "女")]
        Woman = 3
    }
}