﻿namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 微信音乐回复关键字
    /// </summary>
    public class WeiChat_KeyWordMusicContent : WeiChat_KeyWordContentTypeBase
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        public string Description { get; set; }
        public string MusicUrl { get; set; }
        public string HQMusicUrl { get; set; }
        public string ThumbMediaId { get; set; }

    }
}