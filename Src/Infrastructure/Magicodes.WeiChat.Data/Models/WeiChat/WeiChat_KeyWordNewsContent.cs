﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 微信新闻（单图文或多图文）回复关键字
    /// </summary>
    public class WeiChat_KeyWordNewsContent : WeiChat_KeyWordContentTypeBase
    {
        public WeiChat_KeyWordNewsContent() : base()
        {
            Articles = new List<WeiChat_KeyWordNewsArticle>();
        }
        [NotMapped]
        [Display(Name = "图文消息标题")]
        public string Title
        {
            get
            {
                return Articles.FirstOrDefault() == null ? "" : Articles.FirstOrDefault().Title;
            }
        }
        private string picUrl;
        [Display(Name = "图片路径")]
        [NotMapped]
        public string PicUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(picUrl))
                {
                    return picUrl;
                }
                return Articles.FirstOrDefault().PicUrl;
            }
            set
            {
                picUrl = value;
            }
        }
        /// <summary>
        /// 文章列表
        /// </summary>
        public virtual ICollection<WeiChat_KeyWordNewsArticle> Articles { get; set; }
    }
}