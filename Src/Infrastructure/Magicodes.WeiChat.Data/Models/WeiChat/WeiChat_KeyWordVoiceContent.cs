﻿namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 微信语音回复关键字
    /// </summary>
    public class WeiChat_KeyWordVoiceContent : WeiChat_KeyWordContentTypeBase
    {
        public string VoiceMediaId { get; set; }

    }
}