﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 自定义菜单版本历史
    /// </summary>
    public class WeiChat_Menu : WeiChat_TenantBase<int>
    {
        public string MenuData { get; set; }
        public string Remark { get; set; }
        /// <summary>
        /// 是否为当前菜单
        /// </summary>
        public bool IsCurrent { get; set; }

        public string Result { get; set; }
    }
}
