﻿using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models
{
    /// <summary>
    /// 多客服信息
    /// </summary>
    public class WeiChat_KFCInfo : WeiChat_TenantBase<int>
    {
        /// <summary>
        /// 客服账号
        /// </summary>
        [Display(Name = "客服账号")]
        public string Account { get; set; }

        /// <summary>
        /// 客服昵称
        /// </summary>
        [Display(Name = "客服昵称")]
        public string NickName { get; set; }

        /// <summary>
        /// 客服工号
        /// </summary>
        [Display(Name = "客服工号")]
        public string JobNumber { get; set; }

        /// <summary>
        /// 客服头像
        /// </summary>
        [Display(Name = "客服头像")]
        public string HeadImgUrl { get; set; }
    }
}