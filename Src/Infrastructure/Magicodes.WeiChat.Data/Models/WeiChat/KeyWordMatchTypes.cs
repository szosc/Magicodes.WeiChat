﻿using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 匹配类型
    /// </summary>
    public enum KeyWordMatchTypes
    {
        /// <summary>
        /// 包含
        /// </summary>
        [Display(Name = "包含")]
        Contains = 0,
        /// <summary>
        /// 等于
        /// </summary>
        [Display(Name = "等于")]
        Equals = 1
    }
}