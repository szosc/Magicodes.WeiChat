﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : Site_ResourceType.cs
//          description :
//  
//          created by 李文强 at  2016/10/06 10:42
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub：https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Magicodes.WeiChat.Data.Models.Site
{
    /// <summary>
    ///     站点资源类型
    /// </summary>
    public class Site_ResourceType : Site_ResourceBase
    {
        /// <summary>
        ///     标题（名称）
        /// </summary>
        [MaxLength(50)]
        [Display(Name = "名称")]
        [Required]
        public string Title { get; set; }

        /// <summary>
        ///     站点资源类型
        /// </summary>
        [Display(Name = "类型")]
        public SiteResourceTypes ResourceType { get; set; }

        /// <summary>
        ///     是否为系统资源
        /// </summary>
        public bool IsSystemResource { get; set; }

        /// <summary>
        ///     封面
        /// </summary>
        [NotMapped]
        public string Cover { get; set; }
    }
}