﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : Site_NewsArticle.cs
//          description :
//  
//          created by 李文强 at  2016/10/06 10:42
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub：https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Magicodes.WeiChat.Data.Models.Site
{
    /// <summary>
    ///     多图文文章
    /// </summary>
    public class Site_NewsArticle : WeiChat_TenantBase<long>
    {
        [Display(Name = "文章")]
        public Guid SiteArticleId { get; set; }

        [Display(Name = "文章")]
        [ForeignKey("SiteArticleId")]
        public Site_Article Article { get; set; }

        [Display(Name = "封面图片显示在正文中")]
        public bool IsShowInText { get; set; }

        [Display(Name = "封面")]
        [ForeignKey("SiteImageId")]
        public Site_Image FrontCoverImage { get; set; }

        [Display(Name = "封面")]
        public Guid SiteImageId { get; set; }
    }
}