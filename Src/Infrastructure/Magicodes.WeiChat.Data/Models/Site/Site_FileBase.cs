﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : Site_FileBase.cs
//          description :
//  
//          created by 李文强 at  2016/10/06 10:42
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub：https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System;
using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.Site
{
    public class Site_FileBase : Site_ResourceBase
    {
        public Site_FileBase()
        {
            Id = Guid.NewGuid();
        }

        /// <summary>
        ///     类型Id
        /// </summary>
        [Display(Name = "标签")]
        public Guid ResourcesTypeId { get; set; }

        /// <summary>
        ///     名称
        /// </summary>
        [Required]
        [MaxLength(225)]
        [Display(Name = "名称")]
        public string Name { get; set; }

        [MaxLength(500)]
        [Display(Name = "站内地址")]
        public virtual string SiteUrl { get; set; }

        [MaxLength(500)]
        [Display(Name = "地址")]
        public virtual string Url { get; set; }
    }
}