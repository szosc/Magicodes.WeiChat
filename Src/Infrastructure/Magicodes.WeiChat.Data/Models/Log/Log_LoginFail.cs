﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : Log_LoginFail.cs
//          description :
//  
//          created by 李文强 at  2016/10/06 10:34
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub：https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System;
using System.ComponentModel.DataAnnotations;
using Magicodes.WeiChat.Data.Models.Interface;

namespace Magicodes.WeiChat.Data.Models.Log
{
    /// <summary>
    ///     登录失败
    /// </summary>
    public class Log_LoginFail : ITenantId
    {
        [Key]
        public long Id { get; set; }

        public string LoginName { get; set; }
        public string Password { get; set; }

        [Display(Name = "客户端IP")]
        public string ClientIpAddress { get; set; }

        [Display(Name = "浏览器信息")]
        public string BrowserInfo { get; set; }

        [Display(Name = "客户端电脑名称")]
        public string ClientName { get; set; }

        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        ///     租户Id
        /// </summary>
        public int TenantId { get; set; }
    }
}