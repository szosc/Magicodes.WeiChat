﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : Log_MemberAccess.cs
//          description :
//  
//          created by 李文强 at  2016/10/06 10:34
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub：https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.Log
{
    /// <summary>
    ///     会员访问日志
    /// </summary>
    public class Log_Access : WeiChat_WeChatBase<long>
    {
        [Display(Name = "请求路径")]
        [MaxLength(500)]
        public string RequestUrl { get; set; }

        [Display(Name = "客户端IP")]
        [MaxLength(20)]
        public string ClientIpAddress { get; set; }

        [MaxLength(500)]
        [Display(Name = "浏览器信息")]
        public string BrowserInfo { get; set; }

        [Display(Name = "执行时间")]
        public long ExecutionDuration { get; set; }

        [Display(Name = "HTTP方法")]
        [MaxLength(20)]
        public string HttpMethod { get; set; }
    }
}