﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : Log_AdminBase.cs
//          description :
//  
//          created by 李文强 at  2016/10/06 10:34
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub：https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System;
using System.ComponentModel.DataAnnotations;
using Magicodes.WeiChat.Data.Models.Interface;

namespace Magicodes.WeiChat.Data.Models.Log
{
    /// <summary>
    ///     后台日志基类
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public abstract class Log_AdminBase<TKey> : IAdminCreate<string>, ITenantId
    {
        [Key]
        public TKey Id { get; set; }

        [MaxLength(128)]
        [Display(Name = "创建人")]
        public string CreateBy { get; set; }

        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        ///     租户Id
        /// </summary>
        public int TenantId { get; set; }
    }
}