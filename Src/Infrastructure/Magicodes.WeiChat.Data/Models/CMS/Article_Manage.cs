﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models.CMS
{
    /// <summary>
    /// 文章管理
    /// </summary>
    public class Article_Manage: WeiChat_TenantBase<long>
    {
        [Display(Name = "所属栏目")]
        public long? ColumnId { get; set; }

        /// <summary>
        /// 所属栏目
        /// </summary>
        [ForeignKey("ColumnId")]
        public Column_Manage ColumnManage { get; set; }

        /// <summary>
        /// 文章标题
        /// </summary>
        [Display(Name = "文章标题")]
        public string Title { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        [Display(Name = "简介")]
        public string Intro { get; set; }

        /// <summary>
        /// 文章作者
        /// </summary>
        [Display(Name = "发布人")]
        public string Autor { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        [Display(Name = "发布时间")]
        public DateTime? Release_Time { get; set; }

        /// <summary>
        /// 文章内容
        /// </summary>
        [Display(Name = "文章内容")]
        public string Content { get; set; }
    }
}
