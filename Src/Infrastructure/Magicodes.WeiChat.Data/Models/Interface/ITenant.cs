﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models.Interface
{
    public interface ITenant<Tkey>
    {
        Tkey Id { get; set; }
        /// <summary>
        /// 租户名称
        /// </summary>
        string Name { get; set; }
        string Remark { get; set; }
    }
}
