﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : WebApiExceptionFilter.cs
//          description :
//  
//          created by 李文强 at  2016/10/06 10:23
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub：https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;
using Magicodes.Logger;
using Magicodes.WeiChat.Infrastructure.Logging;

namespace Magicodes.WeiChat.Infrastructure.WebApiExtension.Filters
{
    public class WebApiExceptionFilter : ExceptionFilterAttribute
    {
        private readonly LoggerBase _logger = Loggers.Current.DefaultLogger; //LogManager.GetCurrentClassLogger();

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            _logger.LogException(actionExecutedContext.Exception);
            if (HttpContext.Current.IsDebuggingEnabled)
                actionExecutedContext.Response =
                    actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                        actionExecutedContext.Exception.Message, actionExecutedContext.Exception);
            else
                actionExecutedContext.Response =
                    actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                        "服务器出现错误，请联系管理员！");
        }
    }
}