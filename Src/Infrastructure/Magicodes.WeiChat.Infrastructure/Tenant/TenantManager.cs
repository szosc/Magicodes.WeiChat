﻿using Magicodes.WeiChat.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.DynamicFilters;
using Magicodes.WeiChat.Data;

namespace Magicodes.WeiChat.Infrastructure.Tenant
{
    /// <summary>
    /// 租户管理器
    /// </summary>
    public class TenantManager: ThreadSafeLazyBaseSingleton<TenantManager>
    {
        private const string tenantFilterName = "TenantEntryFilter";
        /// <summary>
        /// 启用多租户筛选器
        /// </summary>
        /// <param name="db"></param>
        /// <param name="tenantId"></param>
        public void EnableTenantFilter(AppDbContext db,int tenantId)
        {
            db.EnableFilter(tenantFilterName);
            //设置多租户过滤
            db.SetFilterScopedParameterValue(tenantFilterName, "tenantId", tenantId);
        }
        /// <summary>
        /// 禁用多租户筛选器
        /// </summary>
        /// <param name="db"></param>
        public void DisableTenantFilter(AppDbContext db)
        {
            db.DisableFilter(tenantFilterName);
            db.ClearScopedParameters();
        }
    }
}
