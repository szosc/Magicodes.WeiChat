﻿using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Infrastructure.Events
{
    /// <summary>
    /// 公众号事件参数基类（包含部分默认全局函数，以便表达式中调用）
    /// </summary>
    public abstract class WeChatEventDataBase : IEventData
    {
        /// <summary>
        /// 获取粉丝信息
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        public virtual WeiChat_User GetWeChatUserByOpenId(string openId)
        {
            using (var db = new AppDbContext())
            {
                return db.WeiChat_Users.FirstOrDefault(p => p.OpenId == openId);
            }
        }

        /// <summary>
        /// 获取当前粉丝信息
        /// </summary>
        /// <returns></returns>
        public virtual WeiChat_User GetCurrentWeChatUserInfo()
        {
            return WeiChatApplicationContext.Current.WeiChatUser;
        }
    }
}
