﻿using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Data.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Infrastructure.Identity
{
    public static class IdentityExtension
    {
        public static ITenant<int> GetTenantInfo(this IIdentity identity)
        {
            return WeiChatApplicationContext.Current.TenantInfo;
        }
    }
}
