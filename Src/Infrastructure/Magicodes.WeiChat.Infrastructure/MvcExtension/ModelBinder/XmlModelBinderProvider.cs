﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Infrastructure.MvcExtension.ModelBinder
{
    /// <summary>
    /// 明确告知MVC当客户的请求格式为text/xml时，应该使用XmlModelBinder
    /// 注册方式如下：
    ///protected void Application_Start()
    ///{
    ///    ModelBinderProviders.BinderProviders.Insert(0, new XmlModelBinderProvider());
    ///}
    /// </summary>
    public class XmlModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(Type modelType)
        {
            var contentType = HttpContext.Current.Request.ContentType.ToLower();
            if (contentType != "text/xml")
            {
                return null;
            }
            return new XmlModelBinder();
        }
    }
}
