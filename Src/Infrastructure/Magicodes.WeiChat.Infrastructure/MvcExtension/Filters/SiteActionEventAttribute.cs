﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Infrastructure.MvcExtension.Filters
{
    ///// <summary>
    ///// 站点操作事件配置
    ///// </summary>
    //public class SiteActionEventAttribute : ActionFilterAttribute
    //{
    //    /// <summary>
    //    /// 名称
    //    /// </summary>
    //    public string Name { get; set; }

    //    /// <summary>
    //    /// 显示名称
    //    /// </summary>
    //    public string DisplayName { get; set; }

    //    /// <summary>
    //    /// 描述
    //    /// </summary>
    //    public string Description { get; set; }

    //    /// <summary>
    //    /// 触发器
    //    /// </summary>
    //    public SiteActionEventTriggers Trigger { get; set; }

    //    /// <summary>
    //    /// 数据类型
    //    /// </summary>
    //    public List<Type> EventDataTypes { get; set; }
    //}
}
