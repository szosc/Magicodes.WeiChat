﻿using Magicodes.WeChat.SDK;

namespace Test
{
    public class WeChatConfig : IWeChatConfig
    {
        public string AppId { get; set; }
        public string AppSecret { get; set; }
        public string WeiXinAccount { get; set; }
        public string Token { get; set; }
    }
}
