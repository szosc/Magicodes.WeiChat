﻿using Magicodes.WeChat.SDK;
using Magicodes.WeChat.SDK.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public abstract class TestBase
    {
        static TestBase()
        {
            WeChatSDKBuilder.Create()
                //设置日志记录
                .WithLoggerAction((tag, message) =>
                {
                    Console.WriteLine(string.Format("Tag:{0}\tMessage:{1}", tag, message));
                })
                //注册Key。不再需要各个控制注册
                .Register(WeChatFrameworkFuncTypes.GetKey, model =>
                {
                    return "wx941100f605a8c3bd";
                }
                )
                //注册获取配置函数：根据Key获取微信配置（加载一次后续将缓存）
                .Register(WeChatFrameworkFuncTypes.Config_GetWeChatConfigByKey,
                model =>
                {
                    var arg = model as WeChatApiCallbackFuncArgInfo;
                    return new WeChatConfig()
                    {
                        AppId = "wx941100f605a8c3bd",
                        AppSecret = "ffb2f8569c76f45c5bd667227b2a8c2d"
                    };
                })
                .Build();
        }

        //public static Stream GetInputFile(string filename)
        //{
        //    Assembly thisAssembly = Assembly.GetExecutingAssembly();
        //    string path = "Magicodes.WeChat.SDK.Test.Resource";
        //    return thisAssembly.GetManifestResourceStream(path + "." + filename);
        //}
    }
}
